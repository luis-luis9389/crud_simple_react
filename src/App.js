import React, { useState } from 'react';
import shortid from 'shortid'


function App() {

  const [tarea, guardarTarea] = useState('')
  const [tareas, guardarTareas] = useState([])
  const [error, guardarError] = useState(false)
  const [edicion, guardarEdicion] = useState(false)

  //state para editar por medio de id
  const [id, guardarId] = useState('')

  const agregarTarea = e => {
    e.preventDefault()

    if (!tarea.trim()) {
      guardarError('Escribe algo por favor')
      return
    }
    guardarError(false)
    guardarTarea('')

    guardarTareas([
      ...tareas,
      { id: shortid.generate(), nombreTarea: tarea }
    ])
  }

  //eliminando tareas
  const eliminarTarea = id => {
    const arrayFiltrado = tareas.filter(tarea => tarea.id !== id)
    guardarTareas(arrayFiltrado)
  }


  const editar = tarea => {
    guardarEdicion(true)
    guardarTarea(tarea.nombreTarea)
    guardarId(tarea.id)
  }

  const editarTarea = e => {
    e.preventDefault()
    if (!tarea.trim()) {
      guardarError('Escribe algo por favor')
      return
    }
    
    const arrayEditado = tareas.map(item => item.id === id ? {id:id, nombreTarea:tarea} : item)
    guardarTareas(arrayEditado)
    guardarEdicion(false)
    guardarTarea('')
    guardarId('')
    guardarError(false)
  }

  return (
    <div className="container mt-5">
      <h1 className="text-center">CRUD APP</h1>
      <hr />
      <div className="row">

        <div className="col-8">
          <h4 className="text-center">Lista de Tareas</h4>
          <ul className="list-group">
            {

              tareas.length === 0 ? 
              (
                <li className="list-group-item">No hay tareas</li>
              )
              :
              (
                tareas.map(tarea => (
                  <li className="list-group-item" key={tarea.id}>
                    <span className="lead">{tarea.nombreTarea}</span>
  
                    <button
                      className="btn btn-sm btn-danger float-right mx-2"
                      onClick={()=> eliminarTarea(tarea.id)}
                    >Eliminar</button>
                    <button
                      onClick={()=> editar(tarea)}
                      className="btn btn-sm btn-warning float-right"
                    >Editar</button>
                  </li>
                ))
              )
            }
          </ul>
        </div>

        <div className="col-4">
          <h4 className="text-center">
            {
              edicion ? 'Editar Tarea' : 'Agregar Tarea'
            }
      </h4>
          <form
            onSubmit={edicion ? editarTarea : agregarTarea}
          >

            {
              error ? <span className="text-danger">{error}</span> : null
            }

            <input
              type="text"
              className="form-control mb-2"
              placeholder="Ingrese Tarea"
              onChange={e => guardarTarea(e.target.value)}
              value={tarea}
            />
            {
              edicion ? 
              (
            <button className="btn btn-warning btn-block" type="submit">Editar</button>
              )
              :
              (
                <button className="btn btn-dark btn-block" type="submit">Agregar</button>
              )
            }
          </form>
        </div>

      </div>
    </div>
  );
}

export default App;
